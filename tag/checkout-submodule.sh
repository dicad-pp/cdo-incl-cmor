#!/bin/sh
#Define submodules with versions relevant for own tag:
submodules="../configuration/cmip6/cmip6-cmor-tables ../configuration/cmip6/cmip6-mapping-tables ../software/cmor"
cdosubmodule="../software/cdo/cdo-git"
#Read the tag and commits from a commit file
#. ../recent_commits.h
. ./commits_cdo-2024-02-15_cmor3.8.0.h
#
#onlycdo=Y
home=$(pwd)
if [ -z "$onlycdo" ]; then
  submoduleno=0
  for submodule in $submodules; do
    cd $submodule
#Get all remote tags:
    git fetch
    if [ ! -z "${tags}" ]; then
#Create new branch in submodule:
      git checkout -B $(echo ${tags[$submoduleno]} | cut -d '/' -f 3) ${tags[$submoduleno]}
    else
#show all/last remote tags:
      lastTagTemp=$(git ls-remote --tags | tail -1)
      lastRemoteTag=$(echo $lastTagTemp | cut -d ' ' -f 2)
      lastTag=$(echo $lastRemoteTag | cut -d '/' -f 3)
#Create new branch in submodule:
      git checkout -B $lastTag $lastRemoteTag
    fi
    cd ../
#    git commit -a -m "Changed to new ${submodule} tag"
    cd $home
    submoduleno=$((submoduleno+1))
  done
fi
#
cd $cdosubmodule
git fetch
#For bandit_tests:
git submodule init
git submodule update
#CDI:
cd libcdi
git fetch
git checkout -B $cdotag ${cditagcommit}
cd ../
git add libcdi
git commit -a -m "Changed to new cdi tag"

git checkout -B $cdotag ${cdotagcommit}
cd ../
git add cdo-git
git commit -a -m "Changed to new cdo tag"
cd ../../
