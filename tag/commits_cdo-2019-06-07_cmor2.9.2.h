cdotag="2019-06-07"
cmortag="2.9.2"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="8b95adb67dcf1c6650988d67131ef7a6dc27839a"
cditagcommit="56e9d07997cdb3ec7fcaa5fb9591930cd18b0291"

