cdotag="2023-03-02"
cmortag="3.6.0"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="89f93ef4ebc9b7ce0be985518c62d39fcc74f13b"
cditagcommit="79fb21b7e904315de820c2bc75c4fe7ac10c95c9"

