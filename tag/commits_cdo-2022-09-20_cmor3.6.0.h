cdotag="2022-09-20"
cmortag="3.6.0"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="6e18b06a30aa20e7fd251336089b01c2cf58613e"
cditagcommit="35449c4bb1a6087b818184dd9a630b50ac325b89"

