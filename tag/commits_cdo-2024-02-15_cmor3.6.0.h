cdotag="2024-02-15"
cmortag="3.6.0"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="035b06ab732005dbda10b8ff5090713ebcb3824a"
cditagcommit="42d13a2f7c8711205cab9e7ed71616b89b673fdd"

