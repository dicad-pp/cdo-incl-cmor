cdotag="2024-07-26"
cmortag="3.8.0"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="bfbee806e0511d5cbdbffa437d7c7532a039a1d7"
cditagcommit="4117e86784b0e64bc38d3a29d96b3b28f08dda14"

