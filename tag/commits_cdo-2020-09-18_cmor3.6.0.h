cdotag="2020-09-18"
cmortag="3.6.0"
#Set the tag manually or unset it to use latest:
#submodules="configuration/cmip6/cmip6-cmor-tables configuration/cmip6/cmip6-mapping-tables software/cmor"
tags=("refs/tags/6.8.31" "refs/tags/v1.0.0" "refs/tags/${cmortag}")
#
#CDO:
#
cdotagcommit="d9b9b6febd11f0d500de3518c5543b14dda9d8b0"
cditagcommit="12daf8ba05a2d91455af7a65805a000b354026da"

