table_id: Table Aday
modeling_realm: atmos

frequency: day

cmor_version: 2.6 ! minimum version of CMOR that can read this table
cf_version:   1.4         ! version of CF that output conforms to
project_id:   HAPPI  ! project id
table_date:   07 March 2017 ! date this table was constructed

missing_value: 1.e20      ! value used to indicate a missing value
                          !   in arrays output by netCDF as 32-bit IEEE 
                          !   floating-point numbers (float or real)

baseURL: http://cmip-pcmdi.llnl.gov/CMIP5/dataLocation 
product: output

required_global_attributes: creation_date tracking_id forcing model_id parent_experiment_id parent_experiment_rip branch_time contact institute_id ! space separated required global attribute 

forcings:   N/A Nat Ant GHG SD SI SA TO SO Oz LU Sl Vl SS Ds BC MD OC AA

expt_id_ok: 'AMIP' 'amip'
expt_id_ok: 'AMIP 1959-2015' 'amipHi'
expt_id_ok: 'AMIP 2006-2015' 'amipCD'
expt_id_ok: 'AMIP 2106-2115: 1.5K warmer than preindustrial conditions (1861-1880)' 'amip15'
expt_id_ok: 'AMIP 2106-2115: 2.0K warmer than preindustrial conditions (1861-1880)' 'amip20'
expt_id_ok: 'AMIP 2106-2115: 1.5K warmer than preindustrial conditions (1861-1880); fixed landcover fraction RCP26 2010' 'amip15land-rcp26'
expt_id_ok: 'AMIP 2106-2115: 2.0K warmer than preindustrial conditions (1861-1880); fixed landcover fraction RCP26 2010' 'amip20land-rcp26'
expt_id_ok: 'AMIP 2106-2115: 1.5K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP1-RCP19 2095' 'amip15land-SSP1'
expt_id_ok: 'AMIP 2106-2115: 2.0K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP1-RCP19 2095' 'amip20land-SSP1'
expt_id_ok: 'AMIP 2106-2115: 1.5K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP2-RCP19 2095' 'amip15land-SSP2'
expt_id_ok: 'AMIP 2106-2115: 2.0K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP2-RCP19 2095' 'amip20land-SSP2'
expt_id_ok: 'AMIP 2106-2115: 1.5K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP1-RCP19 2010' 'amip15land-rcp26_UPDATE'
expt_id_ok: 'AMIP 2106-2115: 2.0K warmer than preindustrial conditions (1861-1880); fixed landcover fraction SSP1-RCP19 2010' 'amip20land-rcp26_UPDATE'
expt_id_ok: 'AMIP 2006-2015; fixed landcover fraction SSP1-RCP19 2010' 'amipCD_UPDATE'
expt_id_ok: 'CMIP5 MPI-ESM-LR historical and rcp26 2006-2015: Current decade experiment forced by strong ENSO events' 'amipPDr2'
expt_id_ok: 'CMIP5 MPI-ESM-LR historical and rcp26 2006-2015: Current decade experiment forced by weak ENSO events' 'amipPDr3'
expt_id_ok: 'CMIP5 MPI-ESM-LR historical and rcp26 2106-2115: Future decade experiment 1.5K warmer than preindustrial conditions (1861-1880) forced by strong ENSO events' 'amipFDr2'
expt_id_ok: 'CMIP5 MPI-ESM-LR historical and rcp26 2106-2115: Future decade experiment 1.5K warmer than preindustrial conditions (1861-1880) forced by weak ENSO events' 'amipFDr3'

approx_interval:  1.000000     ! approximate spacing between successive time
                          !   samples (in units of the output time 
                          !   coordinate.

!============
axis_entry: longitude
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    longitude
units:            degrees_east
axis:             X             ! X, Y, Z, T (default: undeclared)
long_name:        longitude
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         lon
valid_min:        0.0         
valid_max:        360.0 
stored_direction: increasing
type:             double
must_have_bounds: yes
!----------------------------------
!


!============
axis_entry: latitude
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    latitude
units:            degrees_north
axis:             Y             ! X, Y, Z, T (default: undeclared)
long_name:        latitude
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         lat
valid_min:        -90.0         
valid_max:        90.0 
stored_direction: increasing
type:             double
must_have_bounds: yes
!----------------------------------
!


!============
axis_entry: plevs
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    air_pressure
units:            Pa
axis:             Z             ! X, Y, Z, T (default: undeclared)
positive:         down         ! up or down (default: undeclared)
long_name:        pressure
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         plev
stored_direction: decreasing
tolerance:        0.001
type:             double
!requested:        100000. 85000. 70000. 50000. 25000. 10000. 5000. 1000.        ! space-separated list of requested coordinates
requested:        85000. 25000. 1000.        ! space-separated list of requested coordinates
must_have_bounds: no
!----------------------------------
!

!============
axis_entry: plevzg
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    air_pressure
units:            Pa
axis:             Z             ! X, Y, Z, T (default: undeclared)
positive:         down         ! up or down (default: undeclared)
long_name:        pressure
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         plev
stored_direction: decreasing
tolerance:        0.001
type:             double
requested:        50000        ! space-separated list of requested coordinates
must_have_bounds: no
!----------------------------------
!

!============
axis_entry: time
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    time
units:            days since ?
axis:             T             ! X, Y, Z, T (default: undeclared)
long_name:        time
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         time
stored_direction: increasing
type:             double
must_have_bounds: yes
!----------------------------------
!


!============
axis_entry: height2m
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    height
units:            m
axis:             Z             ! X, Y, Z, T (default: undeclared)
positive:         up         ! up or down (default: undeclared)
long_name:        height
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         height
valid_min:        1.0         
valid_max:        10.0 
stored_direction: increasing
type:             double
value:            2.            ! of scalar (singleton) dimension 
must_have_bounds: no
!----------------------------------
!

!============
axis_entry: height10m
!============
!----------------------------------
! Axis attributes:
!----------------------------------
standard_name:    height
units:            m
axis:             Z             ! X, Y, Z, T (default: undeclared)
positive:         up         ! up or down (default: undeclared)
long_name:        height
!----------------------------------
! Additional axis information:
!----------------------------------
out_name:         height
valid_min:        1.0         
valid_max:        30.0 
stored_direction: increasing
type:             double
value:            10.            ! of scalar (singleton) dimension 
must_have_bounds: no
!----------------------------------
!

!============
variable_entry:    huss
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     specific_humidity
units:             1
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Near-Surface Specific Humidity
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height2m
out_name:          huss
type:              real
!----------------------------------
!

!============
variable_entry:    hurs
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     relative_humidity
units:             %
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Near-Surface Relative Humidity
comment:           This is the relative humidity with respect to liquid water for T> 0 C, and with respect to ice for T<0 C.
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height2m
out_name:          hurs
type:              real
!----------------------------------
!

!============
variable_entry:    tasmin
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     air_temperature
units:             K
cell_methods:      time: minimum
cell_measures:     area: areacella
long_name:         Daily Minimum Near-Surface Air Temperature
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height2m
out_name:          tasmin
type:              real
!----------------------------------
!

!============
variable_entry:    tasmax
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     air_temperature
units:             K
cell_methods:      time: maximum
cell_measures:     area: areacella
long_name:         Daily Maximum Near-Surface Air Temperature
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height2m
out_name:          tasmax
type:              real
!----------------------------------
!

!============
variable_entry:    tas
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     air_temperature
units:             K
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Near-Surface Air Temperature
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height2m
out_name:          tas
type:              real
!----------------------------------
!

!============
variable_entry:    ts
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_temperature
units:             K
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Temperature
comment:           ""skin"" temperature (i.e., SST for open ocean)
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          ts
type:              real
valid_min:         176.8
valid_max:         339.6
ok_min_mean_abs:   262.8
ok_max_mean_abs:   293.3
!----------------------------------
!

!============
variable_entry:    pr
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     precipitation_flux
units:             kg m-2 s-1
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Precipitation
comment:           at surface; includes both liquid and solid phases from all types of clouds (both large-scale and convective)
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          pr
type:              real
!----------------------------------
!

!============
variable_entry:    psl
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     air_pressure_at_sea_level
units:             Pa
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Sea Level Pressure
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          psl
type:              real
!----------------------------------
!

!============
variable_entry:    ps
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_air_pressure
units:             Pa
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Air Pressure
comment:           not, in general, the same as mean sea-level pressure
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          ps
type:              real
valid_min:         4.791e+04
valid_max:         1.119e+05
ok_min_mean_abs:   9.165e+04
ok_max_mean_abs:   1.019e+05
!----------------------------------
!

!============
variable_entry:    sfcWind
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     wind_speed
units:             m s-1
cell_methods:      time: mean
long_name:         Near-Surface Wind Speed
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height10m
out_name:          sfcWind
type:              real
!----------------------------------
!

!============
variable_entry:    prw
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     atmosphere_water_vapor_content
units:             kg m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Water Vapor Path
comment:           vertically integrated through the atmospheric column
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          prw
type:              real
valid_min:         -0.0006775
valid_max:         78.04
ok_min_mean_abs:   12.75
ok_max_mean_abs:   23.09
!----------------------------------
!

!============
variable_entry:    clt
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     cloud_area_fraction
units:             %
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Total Cloud Fraction
comment:           for the whole atmospheric column, as seen from the surface or the top of the atmosphere. Includes both large-scale and convective cloud.
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time 
out_name:          clt
type:              real
!----------------------------------
!

!============
variable_entry:    prsn
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     snowfall_flux
units:             kg m-2 s-1
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Snowfall Flux
comment:           at surface; includes precipitation of all forms of water in the solid phase
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          prsn
type:              real
!----------------------------------
!

!============
variable_entry:    uas
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     eastward_wind
units:             m s-1
cell_methods:      time: mean
long_name:         Eastward Near-Surface Wind
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height10m
out_name:          uas
type:              real
!----------------------------------
!

!============
variable_entry:    vas
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     northward_wind
units:             m s-1
cell_methods:      time: mean
long_name:         Northward Near-Surface Wind
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time height10m
out_name:          vas
type:              real
!----------------------------------
!

!============
variable_entry:    hfls
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_upward_latent_heat_flux
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Upward Latent Heat Flux
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          hfls
type:              real
positive:          up
!----------------------------------
!

!============
variable_entry:    hfss
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_upward_sensible_heat_flux
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Upward Sensible Heat Flux
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          hfss
type:              real
positive:          up
!----------------------------------
!

!============
variable_entry:    rlds
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_downwelling_longwave_flux_in_air
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Downwelling Longwave Radiation
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          rlds
type:              real
positive:          down
!----------------------------------
!

!============
variable_entry:    rlus
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_upwelling_longwave_flux_in_air
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Upwelling Longwave Radiation
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          rlus
type:              real
positive:          up
!----------------------------------
!

!============
variable_entry:    rlns
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_net_longwave_flux_in_air
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Net Longwave Radiation
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          rlns
type:              real
positive:          up
!----------------------------------
!

!============
variable_entry:    rsds
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_downwelling_shortwave_flux_in_air
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Downwelling Shortwave Radiation
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          rsds
type:              real
positive:          down
!----------------------------------
!

!============
variable_entry:    rsus
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     surface_upwelling_shortwave_flux_in_air
units:             W m-2
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Surface Upwelling Shortwave Radiation
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude time
out_name:          rsus
type:              real
positive:          up
!----------------------------------
!

!============
variable_entry:    va
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     northward_wind
units:             m s-1 
cell_methods:      time: mean
long_name:         Northward Wind
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude plevs time
out_name:          va
type:              real
!----------------------------------
!

!============
variable_entry:    ua
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     eastward_wind
units:             m s-1 
cell_methods:      time: mean
long_name:         Eastward Wind
!----------------------------------
! Additional variable information:
!----------------------------------
dimensions:        longitude latitude plevs time
out_name:          ua
type:              real
!----------------------------------
!

!============
variable_entry:    zg500
!============
modeling_realm:    atmos
!----------------------------------
! Variable attributes:
!----------------------------------
standard_name:     geopotential_height
units:             m
cell_methods:      time: mean
cell_measures:     area: areacella
long_name:         Geopotential Height at 500hPa
!----------------------------------
! Additional variable information:
!----------------------------------
!dimensions:        longitude latitude plevzg time
dimensions:        longitude latitude time
out_name:          zg500
type:              real
!----------------------------------
!

