#!/bin/bash
. ../tag/dockercommits
cdoVers="cdo-${cdotag}"
cmorVers="cmor-${cmortag}"
cd ../application/handson/
./handson_cmip6.sh $cdoVers $cmorVers #>test.log 2>&1
if [ -z $(grep ERR test.log) || -z $(grep Abort test.log) ]; then
  echo "ERR: Test failed";
  exit 1;
fi

correctStoredFiles=$(grep "stored in" test.log | wc -l)
if [ "$correctStoredFiles" -ne 14 ]; then
  echo "$((14-correctStoredFiles)) files have not been succesfully cmorized."
fi
rm -rf CMIP6
rm .CHUNK*
rm mapping_table.txt
rm .cdocmorinfo
