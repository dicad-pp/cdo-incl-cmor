#!/bin/sh
sudo apt-get install linux-image-generic
sudo apt-get install linux-image-extra-$(uname -r) linux-image-extra-virtual
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
lsb_release -cs
uname -a
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu trusty stable"
sudo apt-get update
sudo apt-get install docker-ce
#  sudo add-apt-repository --remove "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#  apt-cache policy docker-ce
echo 'deb http://cz.archive.ubuntu.com/ubuntu trusty main' >> /etc/apt/sources.list.d/docker.list
sudo apt-get install docker-ce
docker
systemctl start docker
sudo docker run -it --name my-first-container alpine:3.4 /bin/echo "Hello World"
