#!/bin/bash
sudo docker stop cdocmortest > /dev/null 2>&1 &&  printf "Stopping old container ... \033[0;32mdone\033[0m\n" || printf "Stopping old container ... \033[0;33mnot needed\033[0m\n"
sudo docker rm cdocmortest > /dev/null 2>&1 && printf "Deleting old container ... \033[0;32mdone\033[0m\n" || printf "Deleting old container ... \033[0;33mnot needed\033[0m\n"
#sudo docker run --name=cdocmortest -dt -v "/home/wachsmann/Schreibtisch/cdo-incl-cmor/:/cdo-incl-cmor" --net=host cdocmor-conda-env
sudo docker run --name=cdocmortest -dt --net=host cdocmor-conda-env
docker cp ./Install-cdo-with-cdocmorenv.sh cdocmortest:/ > /dev/null 2>&1 && printf "Copying install script ... \033[0;32mdone\033[0m\n" || { printf "Copying install script ... \033[0;33mfailed\033[0m\n"; exit 1; } 
sleep 1
docker exec -it cdocmortest /bin/bash /Install-cdo-with-cdocmorenv.sh
#
docker commit -m="Cdo with CMOR built" -a "Fabian Wachsmann" cdocmortest cdocmorbuild
