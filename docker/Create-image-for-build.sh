#!/bin/bash
PWD=`pwd`
#docker pull condaforge/miniforge3
docker stop cdocmortest > /dev/null 2>&1 &&  printf "Stopping old container ... \033[0;32mdone\033[0m\n" || printf "Stopping old container ... \033[0;33mnot needed\033[0m\n"
docker rm cdocmortest > /dev/null 2>&1 && printf "Deleting old container ... \033[0;32mdone\033[0m\n" || printf "Deleting old container ... \033[0;33mnot needed\033[0m\n"
sudo docker run --name=cdocmortest -dt --net=host condaforge/miniforge3 /bin/sh
#docker cp ./Install-cdocmorenv-withconda.sh cdocmortest:/  /dev/null 2>&1 && printf "Copying install script ... \033[0;32mdone\033[0m\n" || { printf "Copying install script ... \033[0;33mfailed\033[0m\n"; exit 1; }
docker cp ./Install-cdocmorenv-withconda.sh cdocmortest:/  && printf "Copying install script ... \033[0;32mdone\033[0m\n" || { printf "Copying install script ... \033[0;33mfailed\033[0m\n"; exit 1; }  
#docker exec -it cdocmor-conda /bin/bash
sleep 1
docker exec -it cdocmortest /bin/sh /Install-cdocmorenv-withconda.sh
#
docker commit -m="Cdo with CMOR environment" -a "Fabian Wachsmann" cdocmortest cdocmor-conda-env
#docker save cdocmortest-env >cdocmortest-env-all.tar
