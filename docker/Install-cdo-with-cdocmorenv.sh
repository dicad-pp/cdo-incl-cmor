#!/bin/bash
conda init bash
source activate cdocmorenv
ls cdo-incl-cmor || git clone https://gitlab.dkrz.de/dicad-pp/cdo-incl-cmor.git
git config --global user.email "wachsmann@dkrz.de"
cd /cdo-incl-cmor
git submodule update --init --recursive || echo "failed"
wait
export ACLOCAL_PATH=/usr/share/aclocal:$ACLOCAL_PATH
cd tag
chmod 755 checkout-submodule-docker.sh
bash ./checkout-submodule-docker.sh
cd ../software
chmod 755 install-cdocmor-on-docker.sh
bash ./install-cdocmor-on-docker.sh
