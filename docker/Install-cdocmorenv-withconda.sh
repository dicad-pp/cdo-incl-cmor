#!/bin/sh
conda update -q --all
conda config --set always_yes yes --set changeps1 no
conda install --quiet git conda-build gcc gxx pip autoconf automake libtool compilers make -c conda-forge -y
git clone https://github.com/conda-forge/cdo-feedstock.git
cd cdo-feedstock
git pull origin dev
git checkout origin/dev
cd /
echo "name : cdocmorenv" > env.yaml
echo "channels:" >>env.yaml
echo "  - conda-forge"  >>env.yaml
echo "dependencies:" >>env.yaml
sed -n '/host:/,/run:/{/host:/b;/run:/b;p}' /cdo-feedstock/recipe/meta.yaml >>env.yaml
conda env create -f env.yaml
conda init bash
#conda activate cdocmorenv
