#!/bin/sh
# INSTALL SCRIPT FOR CDO WITH SUPPORT FOR NETCDF, GRIB2, HDF5 AND CMOR on a LINUX platform
# based on the instructions by Mithil Shah (http://www.studytrails.com/blog/install-climate-data-operator-cdo-with-netcdf-grib2-and-hdf5-support/)
# Author: Fabian Wachsmann
#
#You might need to install a fortran compiler, C compiler and a macro environment:
#sudo apt-get install gfortran
#sudo apt-get install gcc
#sudo apt-get install m4
#
mkdir libs4cdo
cd libs4cdo
INSTALLDIR=$(pwd)

#Install CDO with support:
#ZLIB, HDF5, EXPAT, UDUNITS, UUID, NETCDF, JASPER, GRIB_API, CMOR
zlib=N
hdf5=N
expat=N
udunits=N
uuid=N
netcdf=N
jasper=N
gribapi=N
cmor=Y

#ZLIB 1.2.8:
#for unzip and zipping:
zlib_link="http://www.zlib.net/fossils/zlib-1.2.8.tar.gz"
#HDF5 1.8.12
#data model used by netCDF allowing infinity file sizes
hdf5_link="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.8/hdf5-1.8.13/src/hdf5-1.8.13.tar.gz"
#EXPAT 2.2.2
#xml parser (used by cmor2)
expat_link="https://github.com/libexpat/libexpat/releases/download/R_2_2_2/expat-2.2.2.tar.bz2"
#UDUNITS 2.2.25
#changes units of variables
udunits_link="ftp://ftp.unidata.ucar.edu/pub/udunits/udunits-2.2.25.tar.gz"
#UUID 1.6.2
#creates universal unique identifier
uuid_link="https://launchpad.net/ubuntu/+archive/primary/+files/ossp-uuid_1.6.2.orig.tar.gz"
#NETCDF 4.4.1
#data format of climate data
netcdf_link="https://github.com/Unidata/netcdf-c/archive/v4.4.1.tar.gz"
#JASPER 1.9.1
#jpeg compressor
jasper_link="http://www.ece.uvic.ca/~mdadams/jasper/software/jasper-1.900.1.zip"
#GRIB_API 1.14.4
#data format of climate data
gribapi_link="https://software.ecmwf.int/wiki/download/attachments/3473437/grib_api-1.14.4-Source.tar.gz?api=v2"

if [ "$zlib" = "Y" ]; then
#ZLIB 1.2.8
wget ${zlib_link} && tar xf zlib* || exit 1
cd zlib* || exit 1
./configure --prefix=${INSTALLDIR} || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$hdf5" = "Y" ]; then
#HDF5 1.8.12
wget --no-check-certificate ${hdf5_link} && tar xf hdf5* || exit 1
cd hdf5* || exit 1
./configure --with-zlib=${INSTALLDIR} --prefix=${INSTALLDIR} --enable-hl CFLAGS=-fPIC || exit 1
make && make check || exit 1
make install || exit 1
cd ..
fi

if [ "$expat" = "Y" ]; then
#EXPAT 2.2.2
wget --no-check-certificate ${expat_link} && tar xf expat* || exit 1
cd expat* || exit 1
./configure --prefix=${INSTALLDIR} CFLAGS=-fPIC || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$udunits" = "Y" ]; then
#UDUNITS 2.2.25
wget ${udunits_link} && tar xf udunits* || exit 1
cd udunits* || exit 1
CPPFLAGS=-I${INSTALLDIR}/include LDFLAGS=-L${INSTALLDIR}/lib ./configure --prefix=${INSTALLDIR} CFLAGS=-fPIC || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$uuid" = "Y" ]; then
#UUID 1.6.2
wget --no-check-certificate ${uuid_link} && tar xf ossp-uuid* || exit 1
cd uuid*
./configure --prefix=${INSTALLDIR} CFLAGS=-fPIC || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$netcdf" = "Y" ]; then
#NETCDF 4.4.1
wget --no-check-certificate ${netcdf_link} && tar xf v4* || exit 1
cd net*
CPPFLAGS=-I${INSTALLDIR}/include LDFLAGS=-L${INSTALLDIR}/lib ./configure --prefix=${INSTALLDIR} --enable-netcdf-4 CFLAGS=-fPIC || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$jasper" = "Y" ]; then
#JASPER 1.9.1
wget ${jasper_link} && unzip jasper* || exit 1
cd jasper*
./configure --prefix=${INSTALLDIR} CFLAGS=-fPIC || exit 1
make && make check && make install || exit 1
cd ..
fi

if [ "$gribapi" = "Y" ]; then
#GRIB_API 1.14.4
wget --no-check-certificate ${gribapi_link} && tar xf grib* || exit 1
cd grib*
./configure --prefix=${INSTALLDIR} CFLAGS=-fPIC --with-netcdf=${INSTALLDIR} --with-jasper=${INSTALLDIR} || exit 1
make && make install || exit 1
cd ..
fi

if [ "$cmor" = "Y" ]; then
#CMOR 3.2.8
cd ../../cmor-libs/cmor3-lib/cmor3_v328/ || exit 1
CFLAGS=-fPIC CPPFLAGS=-I${INSTALLDIR}/include LDFLAGS=-L${INSTALLDIR}/lib ./configure --prefix=${INSTALLDIR} --with-udunits2=${INSTALLDIR} --with-uuid=${INSTALLDIR} --with-netcdf=${INSTALLDIR} || exit 1
make && make install || exit 1
cd ${INSTALLDIR}/../
fi

#CDO 1.9.3
cd ${INSTALLDIR}/../
cd cdo_gitcmor3branch || exit 1  
make distclean
CPPFLAGS="-I${INSTALLDIR}/include -I${INSTALLDIR}/local/include -I${INSTALLDIR}/local/include/json-c -I${INSTALLDIR}/local/include/cdTime" OPENMP=0 ./configure --prefix=$(pwd)/ --with-cmor=${INSTALLDIR} --with-hdf5=${INSTALLDIR} --with-netcdf=${INSTALLDIR} --with-udunits2=${INSTALLDIR} --with-grib_api=${INSTALLDIR} --with-ossp-uuid=${INSTALLDIR} --disable-util-linux-uuid || exit 1
                                                                                                                               
make -j8 && make install                                                                                                                                                                                                
cd ..
