#!/bin/ksh -l
# 
# Install CDO 1.9.3 with full support on mistral
# based on the work of Jörg Wegner 
#
#
# -----------------------
# Select CDO path:
# -----------------------
home=$(PWD)
CDO="${home}cdo_progs/cdo_11_15_2017/"
# -----------------------
# Switches for the installation:
# -----------------------
# If CMOR versions greater equal 3 should be installed, set to Y:
isCMIP6=N;
# If CMOR needs to be reinstalled, set to Y:
cmorUpdate=Y;
# If CDO needs to be reinstalled set to Y:
cdoUpdate=Y;
# If Intel compiler should be used, set to Y (Intel not possible with CMOR 2 versions):
isIntel=N;
# -----------------------
# Paths to the libraries:
# -----------------------
home=$(pwd);
LPREDIR="/sw/rhel6-x64"

#data model used by netCDF allowing infinity file sizes
HDF5="${LPREDIR}/hdf5/hdf5-1.8.14-threadsafe-gcc48"

#data format of climate data
NETCDF="${LPREDIR}/netcdf/netcdf_c-4.4.0-gcc48"

#For changing units of variables
UDUNITS="${LPREDIR}/util/udunits-2.2.17-gcc48"

#data format of climate data
GRIBAPI="${LPREDIR}/grib_api/grib_api-1.15.0-gcc48/"

#Compression:
SZLIB="${LPREDIR}/sys/libaec-0.3.4-gcc48"

#Graphics:
PROJ="${LPREDIR}/graphics/proj4-4.9.3-gcc48"
# -----------------------
# Load compilers:
# -----------------------
#For CDO versions greater equal 1.9.1 use GCC Compiler greater 4.8.2 (because of C++ Standard):
module load gcc/7.1.0
module load intel/18.0.1
# -----------------------
# Install CMOR:
# -----------------------
#
#   Directory:
#
CMOR="${home}cmor-libs/cmor2_v292/"
if [[ "${isCMIP6}" = "Y" ]]; then
  CMOR="${home}cmor-libs/cmor3-lib/cmor3_v${cmorSubvers}/"
fi
#
#  Installation:
#
cd ${CMOR}

if [[ "${cmorUpdate}" = "Y" ]]; then
  ./configure --prefix=${CMOR}/../built${cmorSubvers} --with-uuid --with-udunits2=${UDUNITS} --with-netcdf=${NETCDF} LIBS=-lossp-uuid
  make install
  #
  # BUG In CMOR less equal 3.2.3:
  # cp -r ../../cmor-libs/cmor3-lib/cmor3_v323/json-c ../../cmor-libs/cmor3-lib/buil${subvers}/include/
fi
# -----------------------
# Install CDO:
# -----------------------
cd ${CDO}

if [[ "${isCMIP6}" = "N" ]]; then
  export CPPFLAGS="-I${home}cmor-libs/cmor2_v292/include/cdTime"
  export LDFLAGS="-Wl,-rpath,${home}cmor-libs/cmor2_v292/lib"
  if [[ "${cdoUpdate}" = "Y" ]]; then
    make distclean;
    CXX=gcc ./configure --prefix=$(pwd)/ --with-cmor=${CMOR}/../built${cmorSubvers} --with-hdf5=${HDF5} --with-netcdf=${NETCDF} --with-udunits2=${UDUNITS} --with-szlib=${SZLIB} --with-proj=${PROJ}
  fi
else
  if [[ "${cdoUpdate}" = "Y" ]]; then
    make distclean;
    if [[ "${isIntel}" = "Y" ]]; then
      export LDFLAGS=-Wl,-rpath,/sw/rhel6-x64/eccodes/eccodes-2.3.0-gcc48/lib
      ./configure --with-eccodes=${ECCODES} --with-netcdf=${NETCDF} --with-hdf5=${HDF5} --with-szlib=${SZLIB} --with-udunits2=${UDUNITS} --with-proj=${PROJ} --with-ossp-uuid  --disable-util-linux-uuid --with-cmor=${CMOR}/../built${cmorSubvers} F77=ifort 'FFLAGS=-g -O2' CXX=icpc 'CXXFLAGS=-g -Wall -O2 -march=core-avx2' CC=icc 'CFLAGS=-g -Wall -O2 -march=core-avx2'
    else
      ./configure CXX=g++ --prefix=$(pwd)/ --with-cmor=${CMOR}../buil${subvers} --with-hdf5=${HDF5} --with-netcdf=${NETCDF} --with-udunits2=${UDUNITS} --with-grib_api=${GRIBAPI} --with-ossp-uuid  --disable-util-linux-uuid
    fi
  fi
fi
make -j8
#
cd ${home}
