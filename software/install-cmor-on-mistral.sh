#!/bin/sh -l
LPREDIR="/sw/rhel6-x64"
NETCDF="${LPREDIR}/netcdf/netcdf_c-4.6.1-gcc64"
UDUNITS="${LPREDIR}/util/udunits-2.2.17-gcc48"
UUID="${LPREDIR}/intel/python/2018_update3/intelpython3/pkgs/libuuid-1.0.3-intel_2/include/uuid/"

LIBS4CMOR="/work/bm0021/cdo_incl_cmor/libs4cmor"
cp ${UUID}uuid.h ${LIBS4CMOR}/include/

module load intel/18.0.1
module load gcc/7.1.0

#JSON-C 0.13.1
#wget https://github.com/json-c/json-c/archive/json-c-0.13.1-20180305.tar.gz && 
#tar -xzf  json-c-0.13.1*.tar.gz || exit 1
#cd json-c-json-c*
#./configure --prefix=${LIBS4CMOR} || exit 1
#make && make install || exit 1
#cd ..

wget https://github.com/PCMDI/cmor/archive/3.6.0.zip && unzip 3.6.0 || exit 1
cd cmor-3.6.0
  wget https://github.com/PCMDI/cmip6-cmor-tables/archive/6.9.32.zip && unzip 6.9.32 || exit 1
  rm -r cmip6-cmor-tables && mv cmip6-cmor-tables-6.9.32 cmip6-cmor-tables
  sed -i 's\rm -f test_bin/test_singletons\#rm -f test_bin/test_singletons\g' Makefile.in
  export LDFLAGS="-Wl,-rpath=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64/lib"
  export FFLAGS="-Wl,-rpath=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64/lib"
 ./configure --prefix=$(pwd)/../built${cmorSubvers} --with-uuid=${LIBS4CMOR} --with-json-c=${LIBS4CMOR} --with-udunits2=${UDUNITS} --with-netcdf=${NETCDF} CC=gcc
make
make test_C
make test_fortran
make install
cd ..
