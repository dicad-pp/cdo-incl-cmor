#!/bin/sh
# 
# Install CDO with full support on mistral
# based on the work of Jörg Wegner 
#
# -----------------------
# Compiler Option:
# -----------------------
compiler="gcc"
isIntel=N
# -----------------------
# Read in the version to install:
# -----------------------
#. ../recent_commits.h
#. ../tag/commits_cdo-2023-03*
CDO_VERS=${cdotag}
CMOR_VERS=${cmortag}
#CDO_VERS="test"
#CMOR_VERS="3.7.3"
# -----------------------
# Set dirs and prefixes:
# -----------------------
CMOR="cmor"
CMOR_PREFIX="$(pwd)/cmorbuilts/cmor-${CMOR_VERS}"
#CMOR_PREFIX="/sw/rhel6-x64/cmor-3.6.0-gcc64/"
CDO="cdo/cdo-git"
#CDO_PREFIX="$(pwd)/cdobuilts/cdo-${CDO_VERS}_cmor-${CMOR_VERS}_${compiler}"
CDO_PREFIX="/work/bm0021/cdo_incl_cmor/cdo-${CDO_VERS}_cmor${CMOR_VERS}_${compiler}"
# -----------------------
# Install Option:
# -----------------------
isCmorUpdate=Y
isCdoUpdate=Y
# -----------------------
# Paths to the libraries:
# -----------------------
home=$(pwd);
LPREDIR="/sw/spack-levante"
#
      
NETCDF="${LPREDIR}/netcdf-c-4.8.1-qk24yp"
NETCDF="${LPREDIR}/netcdf-c-4.9.2-ytsqvg"
HDF5="${LPREDIR}/hdf5-1.12.1-akf2kp"
HDF5="${LPREDIR}/hdf5-1.14.3-hnqib6"
UDUNITS2="${LPREDIR}/udunits-2.2.28-da6pla"
FFTW3="${LPREDIR}/fftw-3.3.10-fnfhvr"
ECCODES="${LPREDIR}/eccodes-2.32.5-ly6tko"
ECCODES="${LPREDIR}/eccodes-2.34.0-hyoyem"
MAGICSPATH="${LPREDIR}/magics-4.9.3-z64bdu"
SZLIB="${LPREDIR}/libaec-1.0.5-r5sdw5"
PROJ="${LPREDIR}/proj-5.2.0-w7auht"
#Libraries for CMOR:
LPREDIRCMOR="/home/k/k204210/libs4cmor"
# -----------------------
# Load compilers:
# -----------------------
#module load gcc/4.8.2
#For CDO versions greater equal 1.9.1
module unload gcc
module unload intel
module load intel-oneapi-compilers/2022.0.1-gcc-11.2.0
module load gcc/11.2.0-gcc-11.2.0

AUTOMAKE=/sw/spack-levante/automake-1.16.3-yu5jvx/bin
export PATH=$PATH:$AUTOMAKE
# -----------------------
# Install CMOR:
# -----------------------
#3.5.0:cp -r /sw/rhel6-x64/intel/python/2018_update3/intelpython3/pkgs/libuuid-1.0.3-intel_2/include/uuid/uuid.h libs4cmor/include/
#uuid and json have to be in the same directory for successfull cmor-installation
#
cd $CMOR
if [[ "${isCmorUpdate}" = "Y" ]]; then
  autoreconf -vfi --no-recursive
  export LD_LIBRARY_PATH="$LD_LBIRARY_PATH:/sw/spack-levante/udunits-2.2.28-da6pla/lib"
  export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/sw/spack-levante/expat-2.4.1-jntvy5/lib"
  export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${UDUNITS2}/lib"
  export LDFLAGS="-Wl,-rpath=$NETCDF/lib -L/sw/spack-levante/expat-2.4.1-jntvy5/lib -lexpat"
  export LDFLAGS="$LDFLAGS -Wl,-rpath,${UDUNITS2}/lib -L${UDUNITS2}/lib -ludunits2"
  export FFLAGS="-Wl,-rpath=$NETCDF/lib"


  if [[ ${CMOR_VERS} == 2* ]]; then
	  export LDFLAGS="$LDFLAGS -Wl,-rpath=/home/k/k204210/conda-envs/cdocmor/lib -L/home/k/k204210/conda-envs/cdocmor/lib -luuid"

	  ./configure \
		  --prefix=${CMOR_PREFIX} \
		  --with-uuid \
		  --with-udunits2=${UDUNITS2} \
		  --with-netcdf=${NETCDF} \
		  CC=gcc LIBS="-luuid -ludunits2" #LIBS=-lossp-uuid
  else
	  if [[ "${isIntel}" = "Y" ]]; then
	      	  ./configure --prefix=${CMOR_PREFIX} \
	      		  --with-uuid=${LPREDIRCMOR} \
	      		  --with-json-c=${LPREDIRCMOR} \
	      		  --with-udunits2=${UDUNITS2} \
	      		  --with-netcdf=${NETCDF} \
	      		  CC=icc 'CFLAGS=-g -Wall -O2 -march=core-avx2'
	  else
		  # CMOR 3.5
	      	  export LDSHARED_FLAGS="-shared -pthread"
	      	  ./configure --prefix=${CMOR_PREFIX} \
	      		  --with-uuid=${LPREDIRCMOR} \
	      		  --with-json-c=${LPREDIRCMOR} \
	      		  --with-udunits2=${UDUNITS2} \
	      		  --with-netcdf=${NETCDF} \
	      		  CC=gcc
	  fi
  fi
  #Test is not up to date:
  sed -i 's\rm -f test_bin/test_singletons\#rm -f test_bin/test_singletons\g' Makefile.in

  make test_C
  make test_fortran
  make install
  #
  # BUG In CMOR less equal 3.2.3:
  # cp -r ../../cmor-libs/cmor3-lib/cmor3_v323/json-c ../../cmor-libs/cmor3-lib/buil${subvers}/include/
fi
cd ${home}
# -----------------------
# Install CDO:
# -----------------------
cd ${CDO}
  if [[ "${isCdoUpdate}" = "Y" ]]; then
    make distclean;
    export LD_LIBRARY_PATH=/sw/spack-levante/udunits-2.2.28-da6pla/lib
    LDFLAGS="$LDFLAGS -L$ECCODES/lib64 -Wl,-rpath,$ECCODES/lib64"
    LDFLAGS="$LDFLAGS -L$MAGICSPATH/lib64 -Wl,-rpath,$MAGICSPATH/lib64"
    LDFLAGS="$LDFLAGS -L$SZLIB/lib64 -Wl,-rpath,$SZLIB/lib64"
    LDFLAGS="$LDFLAGS -Wl,-rpath,$NETCDF/lib"
    LDFLAGS="$LDFLAGS -Wl,-rpath,$HDF5/lib"
    LDFLAGS="$LDFLAGS -Wl,-rpath,$UDUNITS2/lib"
    LDFLAGS="$LDFLAGS -Wl,-rpath,$PROJ/lib"
    LDFLAGS="$LDFLAGS -L$FFTW3/lib -Wl,-rpath,$FFTW3/lib"
    cd libcdi
    ./autogen.sh
    cd ..
    autoreconf -vfi --no-recursive
# CMOR < 3.5
    if [[ ${CMOR_VERS} == 2* ]]; then
              export LDFLAGS="$LDFLAGS -Wl,-rpath=/home/k/k204210/conda-envs/cdocmor/lib -L/home/k/k204210/conda-envs/cdocmor/lib -luuid"
	          ./configure  --prefix=${CDO_PREFIX} \
			  --with-cmor=${CMOR_PREFIX} \
			  --with-hdf5=${HDF5} \
			  --with-netcdf=${NETCDF} \
			  --with-udunits2=${UDUNITS2} \
			  --with-eccodes=${ECCODES} \
			  --with-ossp-uuid=/home/k/k204210/conda-envs/cdocmor	  \
			  --disable-util-linux-uuid \
			  CXX=g++
    else
      export LDFLAGS="$LDFLAGS -Wl,-rpath,${LPREDIRCMOR}/lib/ -L${LPREDIRCMOR}/lib/ -ljson-c"
      export CPPFLAGS="-I${LPREDIRCMOR}/include"

      if [[ "${isIntel}" = "Y" ]]; then
        ./configure --prefix=${CDO_PREFIX} \
	      --with-fftw3 \
	      --with-eccodes=${ECCODES} \
	      --with-netcdf=${NETCDF} \
	      --with-hdf5=${HDF5} \
	      --with-szlib=${SZLIB} \
	      --with-udunits2=${UDUNITS2} \
	      --with-curl \
	      --with-util-linux-uuid=/home/dkrz/k204210/libs4cmor/ \
	      --with-cmor=${CMOR_PREFIX} \
	      F77=ifort 'FFLAGS=-g -O2' \
	      CXX=icpc 'CXXFLAGS=-g -Wall -O2 -march=core-avx2' \
	      CC=icc 'CFLAGS=-g -Wall -O2 -march=core-avx2' \
	      LIBS="-ljson-c -luuid"
      else
        ./configure --prefix=${CDO_PREFIX} \
	      --with-fftw3 \
	      --with-eccodes=${ECCODES} \
	      --with-netcdf=${NETCDF} \
	      --with-hdf5=${HDF5} \
	      --with-szlib=${SZLIB} \
	      --with-udunits2=${UDUNITS2} \
	      --with-curl \
	      --with-util-linux-uuid=${lLPREDIRCMOR} \
	      --with-cmor=${CMOR_PREFIX} \
	      --with-magics=${MAGICSPATH} \
	      --with-proj=${PROJ} \
	      F77=gfortran 'FFLAGS=-g -O2' \
	      CXX=g++ "CXXFLAGS=-g -Wall -O3 -mavx2 -fPIC -I${FFTW3}/include" \
	      CC=gcc 'CFLAGS=-g -Wall -O3  -mavx2 -fPIC -lm' \
	      LIBS="-lm -ljson-c -luuid"
    fi
# CMOR <3.5
#      ./configure --prefix=${CDO_PREFIX} --with-fftw3 --with-eccodes=${ECCODES} --with-netcdf=${NETCDF} --with-hdf5=${HDF5} --with-szlib=${SZLIB} --with-udunits2=${UDUNITS} --with-curl --with-ossp-uuid --disable-util-linux-uuid --with-cmor=${CMOR_PREFIX} --with-proj=${PROJ} FC=gfortran 'FFLAGS=-g -O2' CXX=g++ 'CXXFLAGS=-g -Wall -O3 -march=native -mavx2' CC=gcc 'CFLAGS=-g -Wall -O3 -march=native -mavx2'
    fi
  fi
    make
    #make check
    make install
  #
cd ${home}

