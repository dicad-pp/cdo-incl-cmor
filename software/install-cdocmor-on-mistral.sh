#!/bin/ksh -l
# 
# Install CDO with full support on mistral
# based on the work of Jörg Wegner 
#
# -----------------------
# Compiler Option:
# -----------------------
compiler="gcc"
isIntel=N
# -----------------------
# Read in the version to install:
# -----------------------
. ../recent_commits.h
CDO_VERS=${cdotag}
CMOR_VERS=${cmortag}
# -----------------------
# Set dirs and prefixes:
# -----------------------
CMOR="cmor"
CMOR_PREFIX="$(pwd)/cmorbuilts/cmor-${CMOR_VERS}"
#CMOR_PREFIX="/sw/rhel6-x64/cmor-3.6.0-gcc64/"
CDO="cdo/cdo-git"
CDO_PREFIX="$(pwd)/cdobuilts/cdo-${CDO_VERS}_cmor-${CMOR_VERS}_${compiler}"
CDO_PREFIX="/work/bm0021/cdo_incl_cmor/cdo-${CDO_VERS}_cmor${CMOR_VERS}_${compiler}"
# -----------------------
# Install Option:
# -----------------------
isCmorUpdate=N
isCdoUpdate=Y
# -----------------------
# Paths to the libraries:
# -----------------------
home=$(pwd);
LPREDIR="/sw/rhel6-x64"
#data model used by netCDF allowing infinity file sizes
HDF5="${LPREDIR}/hdf5/hdf5-1.8.14-threadsafe-gcc48"
#data format of climate data
NETCDF="${LPREDIR}/netcdf/netcdf_c-4.6.1-gcc64"
#For changing units of variables
UDUNITS="${LPREDIR}/util/udunits-2.2.17-gcc48"
#Use Gribapi or alternatively eccodes
#GRIBAPI="${LPREDIR}/grib_api/grib_api-1.15.0-gcc48/"
#ECCODES="${LPREDIR}/eccodes/eccodes-2.3.0-gcc48"
ECCODES="${LPREDIR}/eccodes/eccodes-2.6.0-gcc64/"
#Used by Uwe:
SZLIB="${LPREDIR}/sys/libaec-0.3.4-gcc48"
PROJ="${LPREDIR}/graphics/proj5-5.2.0-gcc64"
#Libraries for CMOR:
LPREDIRCMOR="/work/bm0021/cdo_incl_cmor/libs4cmor"
# -----------------------
# Load compilers:
# -----------------------
#module load gcc/4.8.2
#For CDO versions greater equal 1.9.1
module unload gcc
module unload intel/18.0.1
module load intel/18.0.1
#module load gcc/7.1.0 do not use -O3 in cflags
module load gcc/9.1.0-gcc-7.1.0
module load automake/1.16.1 libtool/2.4.6 autoconf/2.69
# -----------------------
# Install CMOR:
# -----------------------
#3.5.0:cp -r /sw/rhel6-x64/intel/python/2018_update3/intelpython3/pkgs/libuuid-1.0.3-intel_2/include/uuid/uuid.h libs4cmor/include/
#uuid and json have to be in the same directory for successfull cmor-installation
#
cd $CMOR
if [[ "${isCmorUpdate}" = "Y" ]]; then
  make distclean
  export LDFLAGS="-Wl,-rpath=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64/lib"
  export FFLAGS="-Wl,-rpath=/sw/rhel6-x64/netcdf/netcdf_c-4.6.1-gcc64/lib"
#Test is not up to date:
  sed -i 's\rm -f test_bin/test_singletons\#rm -f test_bin/test_singletons\g' Makefile.in
  if [[ "${isIntel}" = "Y" ]]; then
    ./configure --prefix=${CMOR_PREFIX} --with-uuid=${LPREDIRCMOR} --with-json-c=${LPREDIRCMOR} --with-udunits2=${UDUNITS} --with-netcdf=${NETCDF} CC=icc 'CFLAGS=-g -Wall -O2 -march=core-avx2'
  else
# CMOR 3.5
    export LDSHARED_FLAGS="-shared -pthread"
    ./configure --prefix=${CMOR_PREFIX} --with-uuid=${LPREDIRCMOR} --with-json-c=${LPREDIRCMOR} --with-udunits2=${UDUNITS} --with-netcdf=${NETCDF} CC=gcc
# CMOR < 3.5
#    ./configure --prefix=${CMOR_PREFIX} --with-uuid --with-udunits2=${UDUNITS} --with-netcdf=${NETCDF} LIBS=-lossp-uuid CC=gcc
  fi
  make test_C
  make test_fortran
  make install
  #
  # BUG In CMOR less equal 3.2.3:
  # cp -r ../../cmor-libs/cmor3-lib/cmor3_v323/json-c ../../cmor-libs/cmor3-lib/buil${subvers}/include/
fi
cd ${home}
# -----------------------
# Install CDO:
# -----------------------
cd ${CDO}
  if [[ "${isCdoUpdate}" = "Y" ]]; then
    make distclean;
    export LDFLAGS="-L/sw/rhel6-x64/numerics/fftw-3.3.7-openmp-gcc64/lib -Wl,-rpath,/sw/rhel6-x64/eccodes/eccodes-2.6.0-gcc64/lib -Wl,-rpath,${LPREDIRCMOR}/lib/ -L${LPREDIRCMOR}/lib/ -ljson-c -Wl,-rpath,/sw/rhel6-x64/numerics/fftw-3.3.7-openmp-gcc64/lib -lfftw3"
    export CPPFLAGS="-I${LPREDIRCMOR}/include"
    cd libcdi
    ./autogen.sh
    cd ..
    autoreconf -vfi --no-recursive
# CMOR < 3.5
    if [[ "${isIntel}" = "Y" ]]; then
      ./configure --prefix=${CDO_PREFIX} --with-fftw3 --with-eccodes=${ECCODES} --with-netcdf=${NETCDF} --with-hdf5=${HDF5} --with-szlib=${SZLIB} --with-udunits2=${UDUNITS} --with-curl --with-util-linux-uuid=/home/dkrz/k204210/libs4cmor/ --with-cmor=${CMOR_PREFIX} F77=ifort 'FFLAGS=-g -O2' CXX=icpc 'CXXFLAGS=-g -Wall -O2 -march=core-avx2' CC=icc 'CFLAGS=-g -Wall -O2 -march=core-avx2' LIBS="-ljson-c -luuid"
    else
      ./configure --prefix=${CDO_PREFIX} --with-fftw3 --with-eccodes=${ECCODES} --with-netcdf=${NETCDF} --with-hdf5=${HDF5} --with-szlib=${SZLIB} --with-udunits2=${UDUNITS} --with-proj=${PROJ} --with-curl --with-util-linux-uuid=${lLPREDIRCMOR} --with-cmor=${CMOR_PREFIX} FC=gfortran 'FFLAGS=-g -O2' CXX=g++ 'CXXFLAGS=-g -Wall -O3 -march=native -mavx2 -fPIC -I/sw/rhel6-x64/numerics/fftw-3.3.7-openmp-gcc64/include' CC=gcc 'CFLAGS=-g -Wall -O3 -march=native -mavx2 -fPIC -lm' LIBS="-ljson-c -luuid"
# CMOR <3.5
#      ./configure --prefix=${CDO_PREFIX} --with-fftw3 --with-eccodes=${ECCODES} --with-netcdf=${NETCDF} --with-hdf5=${HDF5} --with-szlib=${SZLIB} --with-udunits2=${UDUNITS} --with-curl --with-ossp-uuid --disable-util-linux-uuid --with-cmor=${CMOR_PREFIX} --with-proj=${PROJ} FC=gfortran 'FFLAGS=-g -O2' CXX=g++ 'CXXFLAGS=-g -Wall -O3 -march=native -mavx2' CC=gcc 'CFLAGS=-g -Wall -O3 -march=native -mavx2'
    fi
    make -j 40
    make check
    make install
  fi
#
cd ${home}

