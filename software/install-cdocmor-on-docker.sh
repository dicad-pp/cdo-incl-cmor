#!/bin/bash
set -e
# 
# Install CDO with full support on mistral
# based on the work of Jörg Wegner 
#
# -----------------------
# Compiler Option:
# -----------------------
compiler="gcc"
isIntel=N
# -----------------------
# Read in the version to install:
# -----------------------
. ../tag/dockercommits
CDO_VERS=${cdotag}
CMOR_VERS=${cmortag}
# -----------------------
# Set dirs and prefixes:
# -----------------------
CMOR="cmor"
CMOR_PREFIX="/usr/local/software/cmorbuilts/cmor-${CMOR_VERS}"
CDO="cdo/cdo-git"
CDO_PREFIX="/usr/local/software/cdobuilts/cdo-${CDO_VERS}_cmor-${CMOR_VERS}_${compiler}"
# -----------------------
# Install Option:
# -----------------------
isCmorUpdate=N
isCdoUpdate=Y
# -----------------------
# Paths to the libraries:
# -----------------------
home=$(pwd);
LPREDIR="/opt/conda/envs/cdocmorenv/"
#
cd $CMOR
if [[ "${isCmorUpdate}" = "Y" ]]; then
  echo "... Try to distclean CMOR repo"
#  make distclean
  echo "... Try to reconf CMOR repo"
    autoreconf --force --install
  export LDFLAGS="-Wl,-rpath=${LPREDIR}/lib"
  export FFLAGS="-Wl,-rpath=${LPREDIR}/lib"
  if [[ "${isIntel}" = "Y" ]]; then
    echo "... Try to configure CMOR repo"
    ./configure --prefix=${CMOR_PREFIX} --with-uuid --with-udunits2=${LPREDIR}/ --with-netcdf=${LPREDIR} LIBS=-lossp-uuid CC=icc  'CFLAGS=-g -Wall -O2 -march=core-avx2'
  else
# CMOR 3.5
    export LDSHARED_FLAGS="-shared -pthread"
    echo "... Try to configure CMOR repo"
    ./configure --prefix=${CMOR_PREFIX} --with-util-linux-uuid=${LPREDIR}/ --with-json-c=${LPREDIR}/ --with-udunits2=${LPREDIR}/ --with-netcdf=${LPREDIR}/ CC=gcc
  fi
  echo "... Try to install CMOR repo"
  make install
fi
cd ${home}
# -----------------------
# Install CDO:
# -----------------------
if [[ "${isCdoUpdate}" = "Y" ]]; then
  cd ${CDO}
    export LDFLAGS="-Wl,-rpath,${LPREDIR}/lib -L${LPREDIR}/lib -lhdf5 -ljson-c"
    export CPPFLAGS="-I${LPREDIR}/include/"

    echo "... Try to distclean CDO repo"
#    make distclean;
    cd libcdi
    ./autogen.sh
    cd ..
    echo "... Try to reconf CDO repo"
    autoreconf -vfi --no-recursive --force --install
    echo "... Try to configure CDO repo"
    ./configure --prefix=${CDO_PREFIX} --with-fftw3 --with-libxml2=${LPREDIR} --with-curl=${LPREDIR} --with-proj=${LPREDIR} --with-eccodes=${LPREDIR} --with-netcdf=${LPREDIR} --with-hdf5=${LPREDIR} --with-szlib=${LPREDIR} --with-udunits2=${LPREDIR} --with-util-linux-uuid=${LPREDIR} --with-cmor=${LPREDIR} F77=gfortran 'FFLAGS=-g -O2' CXX=g++ 'CXXFLAGS=-g -Wall -O2 -fPIC -DPIC -fopenmp' CC=gcc 'CFLAGS=-g -Wall -O2 -fPIC -lm' LIBS="-ljson-c -luuid" --disable-dependency-tracking
    echo "... Try to build CDO repo"
    make -j 8
    make install
    make check
fi
cd ${home}
