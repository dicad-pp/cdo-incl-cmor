#
# THE FOLLOWING LINES CONTAIN EXAMPLE EXECUTIONS PERFORMED IN THE CDO CMOR HANDS_ON
# ALL FILES AND CREATED FILES ARE TESTFILES BASED ON MPI-ESM CALCULATIONS. THEY ARE
# - NOT VALID FOR ANY SIMULATION OR EXPERIMENT
# - NOT FOR THE PURPOSE OF ANY PUBLICATION
#
# COPY THAT FILE TO AN APPROPRIATE DIRECTORY AND EXECUTE IT
#
# Creator: Fabian Wachsmann, 10/2017
# wachsmannATdkrz.de
#
#Set CDO Version:
cdoVers=$1
cmorVers=$2
cdoDir="/usr/local/software/cdobuilts"
cdo=${cdoDir}/${cdoVers}_${cmorVers}_gcc/bin/cdo
#cdo=/work/bm0021/cdo_incl_cmor/cdo-test_cmor3.6.0_gcc/bin/cdo
#directories:
cdocmorinfoDir="../../configuration/cmip6/cmip6-cdocmorinfo"
miptableDir="../../configuration/cmip6/cmip6-cmor-tables/Tables"
#format: cat experiment cdocmorcontrol member user source >.cdocmorinfo
cat ${cdocmorinfoDir}/historical_atts ${cdocmorinfoDir}/cdocmorcontrol_atts ${cdocmorinfoDir}/member_atts ${cdocmorinfoDir}/mpi-m_atts ${cdocmorinfoDir}/mpi-esm1-2-hr_atts ${cdocmorinfoDir}/nominalresolution_atts>.cdocmorinfo
echo "height2m=3" >>.cdocmorinfo
#
#Test interface:
${cdo} -v cmor,${miptableDir}/CMIP6_Amon.json example_interface.nc 
#
#Choose tas from 2 infile variables:
${cdo} -v cmor,${miptableDir}/CMIP6_Amon.json,cn=tas example_interface.nc 
#
#PROVIDE METADATA IN INFOTABLE:
#
#Show .cdocmorinfo 
#cat .cdocmorinfo
#Move mip_table_dir attribute to cdocmorinfo
${cdo} -v cmor,Amon,cn=tas example_interface.nc 
#
#MAP MODEL RAW VARIABLES TO CMOR VARIABLES:
#
#Commandline mapping:
${cdo} -v cmor,Amon,cn=tas,c=167,u=K,cm=m example_mapping.grb 
#
#Equal mapping with table:
echo '&parameter cn=tas c=167 u=K cm=m' >>mapping_table.txt
${cdo} -v cmor,Amon,cn=tas,mt=mapping_table.txt example_mapping.grb 

#
#Change infifle variable selector of mapping table:
${cdo} -v cmor,Amon,cn=tas,n=T_2M,mt=mapping_table.txt example_T_2M.nc 
#
#Combine both mapping methods and overwrite table units:
${cdo} -v cmor,Amon,cn=tas,u="K @ 273.15",mt=mapping_table.txt,n=T_2M example_celsius.nc
#exit
#
#Application of a complete mapping-table:
${cdo} -v cmor,Amon,mt=mtPERFECT.txt example_collect.grb 
#
#SPECIAL KEYWORDS:
#
#Change default value of scalar z-coordinate:
${cdo} -v cmor,Amon,za=height2m,mt=mapping_table.txt example_T_3M.nc
#
#USEFUL CDO OPTIONS:
#
#More stdout:
#${cdo} -v cmor,Amon example_interface.nc
#
#No stdout:
#${cdo} -s cmor,Amon example_interface.nc

#cd ../../

