#
# THE FOLLOWING LINES CONTAIN EXAMPLE EXECUTIONS PERFORMED IN THE CDO CMOR HANDS_ON
# ALL FILES AND CREATED FILES ARE TESTFILES BASED ON MPI-ESM CALCULATIONS. THEY ARE
# - NOT VALID FOR ANY SIMULATION OR EXPERIMENT
# - NOT FOR THE PURPOSE OF ANY PUBLICATION
#
# COPY THAT FILE TO AN APPROPRIATE DIRECTORY AND EXECUTE IT
#
# Creator: Fabian Wachsmann, 10/2017
# wachsmannATdkrz.de
#
#CREATE ENVIRONMENT
#
#mkdir cdocmor_handson
#cd cdocmor_handson
#cdoDir=/work/bm0021/cdo_incl_cmor/
#cp -r ${cdoDir}examples ./
#cd examples
#alias cdo=${cdoDir}cdo_handson_cmor2
#cdo=${cdoDir}cdo_handson_cmor2
cdo=/work/bm0021/cdo_incl_cmor/cdo_08_03_2018_cmor2_gcc/src/cdo
#use correct cdocmorinfo:
cp cdocmorinfoCORDEX .cdocmorinfo
#
#Test interface:
${cdo} cmor,cordex_mip_tables/CORDEX_mon example_interface.nc 
#
#Choose tas from 2 infile variables:
${cdo} cmor,cordex_mip_tables/CORDEX_mon,cn=tas example_interface.nc 
#
#PROVIDE METADATA IN INFOTABLE:
#
#Show .cdocmorinfo 
cat .cdocmorinfo
#Move mip_table_dir attribute to cdocmorinfo
${cdo} cmor,mon,cn=tas example_interface.nc 
#
#MAP MODEL RAW VARIABLES TO CMOR VARIABLES:
#
#Commandline mapping:
${cdo} cmor,mon,cn=tas,c=167,u=K,cm=m example_mapping.grb 
#
#Equal mapping with table:
echo '&parameter cn=tas c=167 u=K cm=m' >>mapping_table.txt
${cdo} cmor,mon,cn=tas,mt=mapping_table.txt example_mapping.grb 
#
#Change infifle variable selector of mapping table:
${cdo} cmor,mon,cn=tas,n=T_2M,mt=mapping_table.txt example_T_2M.nc 
#
#Combine both mapping methods and overwrite table units:
${cdo} cmor,mon,cn=tas,u="K @ 273.15",mt=mapping_table.txt example_celsius.nc
#
#Application of a complete mapping-table:
${cdo} cmor,mon,mt=mtPERFECT.txt example_collect.grb 
#
#SPECIAL KEYWORDS:
#
#Change default value of scalar z-coordinate:
${cdo} cmor,mon,szc=height2m_3,mt=mapping_table.txt example_T_3M.nc
#
#USEFUL CDO OPTIONS:
#
#More stdout:
${cdo} -v cmor,mon example_interface.nc
#
#No stdout:
${cdo} -s cmor,mon example_interface.nc

cd ../../

